package com.lhtsoft.dto;

import java.sql.Date;
import java.util.Map;

/**
 * Created by sky on 2018/1/16.
 */
public class Order {
    private String id;
    public String tableId;
    public Date orderDate;
    public String status;
    public String sum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", tableId='" + tableId + '\'' +
                ", orderDate=" + orderDate +
                ", status='" + status + '\'' +
                ", sum='" + sum + '\'' +
                '}';
    }
}
