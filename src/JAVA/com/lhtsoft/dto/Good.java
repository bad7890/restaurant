package com.lhtsoft.dto;

/**
 * Created by sky on 2017/11/17.
 */
public class Good {
    public String id; /*商品编号*/
    public String name; /*商品名称*/
    public String price;/*商品价格*/
    public String unit;/*商品单位*/
    public String image;/*商品图片*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Good{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", unit='" + unit + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
