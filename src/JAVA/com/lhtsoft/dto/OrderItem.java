package com.lhtsoft.dto;

/**
 * Created by sky on 2018/1/26.
 */
public class OrderItem {
    public String goodId;
    public String goodName;
    public String goodNum;
    public String goodPrice;
    public String sum;

    public OrderItem(){}
    public OrderItem(String goodId,String goodName,String goodPrice,String goodNum){
        this.goodId=goodId;
        this.goodName=goodName;
        this.goodPrice=goodPrice;
        this.goodNum=goodNum;
        int a,b;
        a=Integer.parseInt(goodNum);
        b=Integer.parseInt(goodPrice);
        this.sum=String.valueOf(a*b);


    }
    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(String goodNum) {
        this.goodNum = goodNum;
    }

    public String getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(String goodPrice) {
        this.goodPrice = goodPrice;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "goodId='" + goodId + '\'' +
                ", goodName='" + goodName + '\'' +
                ", goodNum='" + goodNum + '\'' +
                ", goodPrice='" + goodPrice + '\'' +
                ", sum='" + sum + '\'' +
                '}';
    }
}
