package com.lhtsoft.dto;

/**
 * Created by sky on 2017/11/14.
 */
public class Table {
    String id;
    String name;
    String seat;
    String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Table{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", seat='" + seat + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
