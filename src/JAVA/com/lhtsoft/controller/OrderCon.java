package com.lhtsoft.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhtsoft.dto.Good;
import com.lhtsoft.dto.Order;
import com.lhtsoft.dto.OrderItem;
import com.lhtsoft.dto.Table;
import com.lhtsoft.service.IGoodService;
import com.lhtsoft.service.IOrderService;
import com.lhtsoft.service.ITableService;
import com.sun.deploy.net.HttpResponse;
import org.apache.ibatis.jdbc.Null;
import org.aspectj.weaver.ast.Or;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sky on 2017/11/14.
 */
@Controller
@RequestMapping("/Order")
public class OrderCon {
    @Autowired
    private IOrderService ios;
    @Autowired
    private ITableService its;
    @Autowired
    private IGoodService igs;

    @RequestMapping("/Top")
    public ModelAndView manage(ModelAndView modelAndView


    ) {

        Map map = new HashMap();
        List<Table> tableList = its.selectAll();

        modelAndView.addObject("table", tableList);
        modelAndView.setViewName("/order/top");
        return modelAndView;

    }

//    @RequestMapping("/ToOrder/{id}")
//    public ModelAndView toOrder(@PathVariable String id,
//                                ModelAndView modelAndView) {
//
//        modelAndView.addObject("tableid",id);
//
//        modelAndView.setViewName("redirect:/Order/Menu.action");
//        return modelAndView;
//    }

    @RequestMapping("/Menu")
    public ModelAndView menu(ModelAndView modelAndView,
                             String tableid,
                             @RequestParam(required = false) String ordertable,
                             HttpServletRequest httpServletRequest

                             ) {
        if (ordertable!=null){
            HttpSession session=httpServletRequest.getSession();
           OrderItem[] orderlist= (OrderItem[]) session.getAttribute(ordertable);
           modelAndView.addObject("orderlist",orderlist);

           tableid=ordertable;
        }
        Map map = new HashMap();
        System.out.println(tableid+"menu");
        List<Good> goodList = igs.selectGood(map);

        modelAndView.addObject("goodList", goodList);
        modelAndView.addObject("tableid", tableid);
        modelAndView.setViewName("/order/menu");
        return modelAndView;
    }

    @RequestMapping("AddMenu")
    public void addmenu(@RequestParam String id,
                        HttpServletResponse response) {
        Good good = igs.get(id);

        OrderItem orderItem = new OrderItem(good.getId(), good.getName(), good.getPrice(), "1");
        ObjectMapper x = new ObjectMapper();

        try {
            String str1 = x.writeValueAsString(orderItem);//将java类对象转换为json字符串
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(str1);
            response.getWriter().close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @RequestMapping("ToDb")
    public ModelAndView todb(String  sum,
                             String tableid,
                             String num,
                             String json,
                             HttpServletRequest request,
                             HttpServletResponse response,
                             ModelAndView modelAndView) throws Exception {
        System.out.println(tableid);
        Table table = its.getTable(tableid);
        table.setStatus("1");
        its.updateTable(table);

        Map map=new HashMap();
        map.put("sum",sum);
        map.put("tableId",tableid);
        ios.newOrder(map);
        int i=0;
        int orderNum=Integer.parseInt(num);
        OrderItem[] orderItemlist=new OrderItem[orderNum];

        JSONArray array = JSONArray.parseArray(json);
        for (Object obj : array) {
            JSONObject jsonObject = (JSONObject) obj;
            OrderItem orderItem = (OrderItem) JSONObject.toJavaObject(jsonObject, OrderItem.class);
            orderItemlist[i++]=orderItem;
        }
        HttpSession session=request.getSession();

        if ( session.getAttribute(tableid)!= null){
            session.removeAttribute(tableid);

        }

        session.setAttribute(tableid,orderItemlist);
        modelAndView.setViewName("/order/top");
        return modelAndView;

    }
    @RequestMapping("GetList")
    public void getlist(String tableId,
                         HttpServletRequest request,
                        HttpServletResponse response ){

        HttpSession session=request.getSession();
        OrderItem[] orderItems=(OrderItem[])session.getAttribute(tableId);
        JSON.toJSONString(orderItems); ObjectMapper x = new ObjectMapper();

        try {
            String str1 = x.writeValueAsString(orderItems);//将java类对象转换为json字符串
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(str1);
            response.getWriter().close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        }

    @RequestMapping("/ToPay/{id}")
    public ModelAndView topay (@PathVariable String id,
                               ModelAndView modelAndView){
        modelAndView.addObject("tableId",id);
        modelAndView.setViewName("/order/pay");
        return modelAndView;

    }
    @RequestMapping("/ToMenu/{ordertable}")
    public  ModelAndView tomenu(@PathVariable String ordertable,
                                ModelAndView modelAndView

            ){
        modelAndView.addObject("ordertable",ordertable);
        modelAndView.setViewName("redirect:/Order/Menu");
        return modelAndView;
    }





    }

