package com.lhtsoft.controller;

import com.lhtsoft.dto.Table;
import com.lhtsoft.service.ITableService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sky on 2017/11/14.
 */
@Controller
@RequestMapping("/Table")
public class TableCon {
    @Autowired
    private ITableService its;

    @RequestMapping("/Manage")
    public ModelAndView manage(ModelAndView modelAndView

    ) {
        Map map = new HashMap();
     

        modelAndView.addObject("Tablelist", its.selectAll());
        modelAndView.setViewName("/table/manage");
        return modelAndView;

    }


    @RequestMapping("/ToUpdate" )
    public String toupdate( @RequestParam(required = false) String id,
                            HttpServletResponse response) {
        Table table=its.getTable(id);
        ObjectMapper x=new ObjectMapper();

        try {
            String str1 = x.writeValueAsString(table);//将java类对象转换为json字符串
            response.setCharacterEncoding("utf-8");
            response.getWriter().print(str1);
            response.getWriter().close();

        } catch (IOException e) {
            e.printStackTrace();
        }

return "true";

    }
    @RequestMapping("/Update" )
    @ResponseBody
    public ModelAndView update( @RequestParam(required = false) String id,
                                @RequestParam(required = false) String name,
                                @RequestParam(required = false) String seat,
                                @RequestParam(required = false) String status,
                          HttpServletResponse response) {
       Table table=new Table();
       ModelAndView modelAndView=new ModelAndView();
       table.setName(name);
       table.setStatus(status);
       table.setId(id);
       table.setSeat(seat);
       System.out.println(table.toString());
        its.updateTable(table);
       modelAndView.setViewName("redirect:/Table/Manage.action");
        return modelAndView ;

    }
    @RequestMapping("/Delete/{id}")
    @ResponseBody
    public ModelAndView delete(
            @PathVariable String id) {

        its.deleteTable(id);

        return new ModelAndView("redirect:/Table/Manage.action");


    }

    @RequestMapping("/Insert")
    @ResponseBody
    public ModelAndView insert(
                               @RequestParam(required = false) String seat,
                               @RequestParam(required = false) String name,
                               @RequestParam(required = false) String status){
        ModelAndView modelAndView=new ModelAndView();


        Table table=new Table();
        table.setSeat(seat);
        table.setName(name);
        table.setStatus(status);

        its.newTable(table);

        modelAndView.setViewName("redirect:/Table/Manage.action");

        return modelAndView;
    }
}