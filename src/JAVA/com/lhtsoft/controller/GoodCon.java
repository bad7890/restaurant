package com.lhtsoft.controller;

import com.lhtsoft.dto.Good;
import com.lhtsoft.service.IGoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sky on 2017/11/14.
 */
@Controller
@RequestMapping("/Good")
public class GoodCon {
    @Autowired
    private IGoodService igs;

    @RequestMapping("/Manage")
    public ModelAndView manage(ModelAndView modelAndView,
                               @RequestParam(required = false) String id,
                               @RequestParam(required = false) String name

    ) {
        Map map = new HashMap();
        if (id != null&&id!="")
            map.put("id", id);
        if (name != null&&name!="") {
            map.put("name", name);
            System.out.println(name);
        }
        modelAndView.addObject("goodlist", igs.selectGood(map));
        modelAndView.setViewName("/good/manage");
        return modelAndView;

    }

    @RequestMapping("/ToUpdate/{id}")
    public ModelAndView toupdate(
                                 @PathVariable String id) {

        ModelAndView modelAndView=new ModelAndView();
         modelAndView.addObject("good",igs.get(id));
         modelAndView.setViewName("/good/update");
        return modelAndView;


    }

    @RequestMapping(value = "/Update" ,method = RequestMethod.POST)
    public ModelAndView update(Good good) {


        igs.updateGood(good);

        return new ModelAndView("redirect:/Good/Manage.action");


    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public ModelAndView delete(
            @PathVariable String id) {

        igs.deleteGood(id);

        return new ModelAndView("redirect:/Good/Manage.action");


    }
    @RequestMapping("/ToInsert")
    public ModelAndView toinsert(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("good",new Good());
        modelAndView.setViewName("/good/insert");
        return modelAndView;

    }

    @RequestMapping("/Insert")
    public ModelAndView insert(ModelAndView modelAndView,
                               Good good){
        igs.newGood(good);
        return new ModelAndView("redirect:/Good/Manage.action");
    }
}