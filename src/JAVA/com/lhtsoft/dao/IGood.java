package com.lhtsoft.dao;

import com.lhtsoft.dto.Good;

import java.util.List;
import java.util.Map;

/**
 * Created by sky on 2017/11/17.
 */
public interface IGood {
     void insert(Good good);  /*插入新的菜*/
     void delete (String id);/*删除菜品*/
     List<Good> select (Map map);/*根据目标查找菜品*/
            Good get(String id);/*根据ID精确查找查找菜品*/
//     void selectall();/*查找全部菜品*/
     void update(Good good);/*更新菜品*/
}
