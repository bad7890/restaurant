package com.lhtsoft.dao;

import com.lhtsoft.dto.Table;

import java.util.List;

/**
 * Created by sky on 2017/11/14.
 */
public interface ITable {
    void insert(Table table);
    void delete(String id);
    void update(Table table);
    List<Table> selectall();
    Table selectone(String id);


}
