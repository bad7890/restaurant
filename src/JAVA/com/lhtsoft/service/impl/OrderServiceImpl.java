package com.lhtsoft.service.impl;

import com.lhtsoft.dao.IOrder;
import com.lhtsoft.dto.Good;
import com.lhtsoft.dto.Order;
import com.lhtsoft.service.IOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by sky on 2018/1/16.
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Resource
    private IOrder iOrder;
    @Override
    public void newOrder(Map map) {
        iOrder.insertOrder(map);


    }
}
