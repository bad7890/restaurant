package com.lhtsoft.service.impl;

import com.lhtsoft.dao.ITable;
import com.lhtsoft.dto.Table;
import com.lhtsoft.service.ITableService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by sky on 2017/11/14.
 */
@Service
public class TableServiceImpl implements ITableService {
    @Resource
    private ITable it;

    @Override
    public void newTable(Table table) {
        it.insert(table);

    }

    @Override
    public void deleteTable(String id) {
        it.delete(id);

    }

    @Override
    public void updateTable(Table table) {
        it.update(table);

    }

    @Override
    public Table getTable(String id) {
        return it.selectone(id);
    }

    @Override
    public List<Table> selectAll() {
       return it.selectall();
    }
}

