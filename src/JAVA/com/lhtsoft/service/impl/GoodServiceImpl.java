package com.lhtsoft.service.impl;

import com.lhtsoft.dao.IGood;
import com.lhtsoft.dto.Good;
import com.lhtsoft.service.IGoodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sky on 2017/11/17.
 */
@Service
public class GoodServiceImpl implements IGoodService
{
    @Resource
    private IGood ig;
    @Override
    public void newGood(Good good) {
        ig.insert(good);

    }

    @Override
    public void deleteGood(String id) {
        ig.delete(id);

    }

    @Override
    public List<Good> selectAll() {
        Map map=new HashMap();


        return ig.select(map);
    }

    @Override
    public List<Good> selectGood(Map map) {

        return ig.select(map);
    }

    @Override
    public void updateGood(Good good) {
        ig.update(good);

    }

    @Override
    public Good get(String id) {
        Good good=ig.get(id);
        return good;
    }
}
