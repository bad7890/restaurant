package com.lhtsoft.service;

import com.lhtsoft.dto.Good;

import java.util.List;
import java.util.Map;

/**
 * Created by sky on 2017/11/17.
 */
public interface IGoodService {
    void newGood(Good good);/*增加新的菜品*/
    void deleteGood(String id );/*删除菜品*/
    List<Good> selectAll();/*查找全部菜品*/
    List<Good> selectGood(Map map);/*根据条件查询菜品*/
    Good get(String id);/*根据ID精确查找查找菜品*/
    void updateGood (Good good);/*更新菜品*/

}
