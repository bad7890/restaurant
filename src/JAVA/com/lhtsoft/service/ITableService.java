package com.lhtsoft.service;

import com.lhtsoft.dto.Table;

import java.util.List;

/**
 * Created by sky on 2017/11/14.
 */
public interface ITableService {
    void newTable(Table table);
    void deleteTable(String id);
    void updateTable(Table table);
    List<Table> selectAll();
    Table getTable(String id);
}
