<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 2017/11/21
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String imgPath = basePath + "fileUpload/temp/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>菜品管理</title>
</head>
<body>
<form method="post">
    编号：<input type="text" name="id"/>
    名称：<input type="text" name="name"/>

    <button type="submit">确认</button>


</form>

<a href="<%=basePath%>Good/ToInsert.action"> 新增</a>
<table>
    <caption>菜品管理</caption>
    <thead>
    <tr>
        <td>编号</td>
        <td>名称</td>
        <td>单价</td>
        <td>单位</td>
        <td>操作</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${goodlist}" var="g">
        <tr>
            <td>${g.id}</td>

            <td>${g.name}</td>
            <td>${g.price}</td>
            <td>${g.unit}</td>
            <td>
                <a href="<%=basePath%>Good/ToUpdate/${g.id}"> 修改</a>
                <a href="<%=basePath%>Good/delete/${g.id}">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
