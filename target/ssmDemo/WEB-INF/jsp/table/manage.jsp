<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 2017/11/21
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String imgPath = basePath + "fileUpload/temp/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="">
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        #bg{
            height: 100%;
            width: 100%;
            float: left;
            position: absolute;
            bottom: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 0;
            display: none;
        }
        #newt{
            height: 300px;
            width: 500px;
            position: absolute;
            bottom: 40%;
            left: 30%;
            z-index: 1;
            background: white;
            display: none;
        }
        #updatet{
            height: 300px;
            width: 500px;
            position: absolute;
            bottom: 40%;
            left: 30%;
            z-index: 1;
            background: white;
            display: none;
        }
        #sum{

        }

    </style>
</head>
<body>

<button id="new">新增</button>
<table>
    <caption>桌位管理</caption>
    <thead>
    <tr>

        <td>桌位名称</td>
        <td>座位数</td>
        <td>状态</td>
        <td>操作</td>

    </tr>
    </thead>
    <tbody>
    <c:forEach items="${Tablelist}" var="t">
        <tr>
            <td>${t.name}</td>

            <td>${t.seat}</td>
            <td>${t.status}</td>
            <td>
                <a href="javascript:void(0)" onclick="updatet(this)" class="${t.id}" > 修改</a>
                <a href="<%=basePath%>Table/Delete/${t.id}.action">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div id="bg"> </div>
<div id="newt">
    <form >
        桌位名称<input type="text" id="tname" name="name">
        <br>
        座位数<input type="text" id="tseat" name="seat">
        <br>
        状态<input type="radio"  name="status" value="0" checked>可用</input>
        <input type="radio"  name="status" value="1">不可用</input>
        <br>
        <button  id="sum">确定</button>

        <div  id="cancel">取消</div>
        <br>
        <span id="objinfo"></span>
    </form>
</div>
<div id="updatet">
    <form >
        <input type="text" id="utid" hidden >
        桌位名称<input type="text" id="utname" name="uname">
        <br>
        座位数<input type="text" id="utseat" name="useat">
        <br>
        状态<input type="radio"  id="s1" name="ustatus" value="0" >可用</input>
        <input type="radio" id="s2" name="ustatus" value="1">不可用</input>
        <br>
        <button  id="usum">确定</button>

        <div  id="ucancel">取消</div>
        <br>

    </form>
</div>
</body>

</html>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    function updatet(tid) {

        $("#bg").css("display","block");
        $("#updatet").css("display","block");
        console.log($(tid).attr("class"));
        $.ajax({
            url: '<%=basePath%>Table/ToUpdate.action',
            async: true,
            type: 'POST',
            data: {
                id:$(tid).attr("class")
            },
            success: function(data){


                var json = eval("("+data+")");//将json类型字符串转换为json对象

                $("#utname").val(json.name);
                $("#utseat").val(json.seat);

                if ( json.status=="0"){
                    $("input[id='s2']").attr("checked",false);
                    $("input[id='s1']").attr("checked",true);
                }else {
                    $("input[id='s1']").attr("checked",false);
                    $("input[id='s2']").attr("checked",true);
                }

                $("#utid").val(json.id);

            }
        });


      }

    $(function(){
        $("#new").click(function () {
            $("#bg").css("display","block");
            $("#newt").css("display","block");
        });

        $("#cancel").click(function () {
            $("#bg").css("display","none");
            $("#newt").css("display","none");
        });
        $("#ucancel").click(function () {
            $("#bg").css("display","none");
            $("#updatet").css("display","none");

        });
    });


</script>
<script >
    //新增桌位
    // 请注意 js 的写法，函数用属性代替
    // 修改默认的 html5 默认的正则表达式验证提示


    $("#sum").click(function () {

// 1 构造异步对象
        var xmlhttp = new XMLHttpRequest();
// 2 监控请求状态变化
        xmlhttp.onreadystatechange = function () {
// 请求成功返回
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if (xmlhttp.responseText == "yes") {
                    $("#objinfo").innerHTML = "插入成功";
                    $("#objinfo").style().color  = "green";

                } else {
                    $("#objinfo").innerHTML  = "失败";
                    $("#objinfo").style().color   = "red";
                }
            }
        }
// 3 设置请求参数
        xmlhttp.open("POST", "<%=basePath%>Table/Insert.action", true);
        xmlhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
// 4 异步请求/seller/isExist?username=" + valUsername, true);
        xmlhttp.send("name="+ $("#tname").val()+"&seat="+$("#tseat").val()+"&status="+$("input[name='status']:checked").val());

    })

</script>
<script>
    //修改桌位信息

    $("#usum").click(function () {

// 1 构造异步对象
        var xmlhttp = new XMLHttpRequest();
// 2 监控请求状态变化
        xmlhttp.onreadystatechange = function () {
// 请求成功返回
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if (xmlhttp.responseText == "yes") {
                    $("#objinfo").innerHTML = "插入成功";
                    $("#objinfo").style().color  = "green";

                } else {
                    $("#objinfo").innerHTML  = "失败";
                    $("#objinfo").style().color   = "red";
                }
            }
        }
// 3 设置请求参数
        xmlhttp.open("POST", "<%=basePath%>Table/Update.action", true);
        xmlhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
// 4 异步请求/seller/isExist?username=" + valUsername, true);
        xmlhttp.send("name="+ $("#utname").val()+"&seat="+$("#utseat").val()+"&status="+$("input[name='status']:checked").val()+"&id="+$("#utid").val());

    })

</script>
