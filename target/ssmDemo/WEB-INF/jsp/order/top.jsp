<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 2018/1/16
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String imgPath = basePath + "fileUpload/temp/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>点餐</title>
    <style>
        html{
            box-sizing: border-box;
        }
        *{
            margin: 0;
            padding: 0;
            list-style: none;
            text-decoration: none;
        }
        *, *:before, *:after {
            box-sizing: inherit;
        }
        #bg{
            height:100%;
            width: 100%;
            float: left;
            position: absolute;
            left: 0;
            display: none;
            z-index: 2;
            background-color: rgba(1,1,1,0);

        }
        .top{
            height:100%;
            width: 100%;
            float: left;
            position: absolute;
            left: 0;
            z-index: 1;

        }
        .alltable{
            float: left;
            height: 100%;
            width: 70%;
            background: blue;
            position: relative;
            overflow: hidden;
        }
        .show{
            height: 100%;
            width: 20%;
            float: left;

            position: relative;
        }
        .table{
            float: left;
            height:150px;
            width: 150px;
            background: #fff9ef;
            margin: 10px;
        }
        .table p{
            text-align: center;
            margin: 40% auto;
        }
        #tool{
            float: left;
            width: 10%;
            height:100%;
            background-color: rgba(23,223,123,0.7);
        }
        #toOrder{
            height:20%;
            width: 30%;
            float: left;
            position: absolute;
            left: 35%;
            top: 40% ;
            background: #00ff61;
            display: none;
            z-index: 3;

        }
        table{
            border-collapse: collapse;
            display: none;
                }
        table tr{border-bottom:solid 1px black;
            overflow: hidden}
        td{
            width: 3%;
            height:40px;
        }
        #defaultmessage{
            display: block;
        }
        #toolbox{
            margin-top: 150px;



        }

        .toolitem{
            width: 80%;
            height: 50px;
            margin: 10px auto;
            margin-bottom: 20px;
            text-align: center;
            line-height: 50px;
            border:1px solid black;
            font-size: 20px;
            color: black;
        }
        #sum{
            width: 130px;
            height: 50px;
            position: absolute;
            right: 0;
            bottom:0;
            text-align: center;
            line-height: 50px;
            font-size: 15px;
        }

    </style>
</head>
<body>


    <div class="show"> <p id="defaultmessage">选择餐桌 开始点菜</p>
        <table>
            <thead>
            <tr>

                <td>菜名</td>
                <td>数量</td>
                <td>价钱</td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <div id="sum">总计：0</div>

    </div>
    <div id="tool">
        <a href=""><div class="toolitem">-</div></a>
        <div class="toolitem">0</div>
        <a href=""> <div  class="toolitem">+</div></a>
        <div id="toolbox">

            <a href=""><div class="toolitem">撤单</div></a>
            <a href="javascript:void(0)" onclick="updateorder()"><div class="toolitem">修改</div></a>
            <a href="javascript:void(0)" onclick="topay()" id="topay"><div id="order" class="toolitem">结账</div></a>

        </div>

    </div>

    <div class="alltable">
        <div id="bg"></div>
        <c:forEach items="${table}" var="t">
            <a  href="javascript:void(0)" onclick="clicktable(${t.status},'${t.id}',this)">
                <div class="table">
                    <p>${t.name}</p>
                    <p hidden class="tid">${t.id}</p>
                </div>
            </a>

        </c:forEach>
    </div>
    <div id="toOrder">
        <a class="toorder">点菜</a>
        <a href="javascript:void(0)" onclick="canceltable()">取消</a>
    </div>
</div>



</body>
</html>
<script  src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
   function clicktable(t,tableid,table) {


//       table.childNodes[1].style.border='5px solid red'
//       .setAttribute('id','selected');
       boxborder(table.childNodes[1]);
       console.log(table.childNodes[1]);


       if ( t!=1){

           $('#bg').css("display", "block");
           $('#toOrder').css("display", "block");
           $('#toOrder').children('.toorder').attr('href','<%=basePath%>Order/ToMenu/'+tableid+'.action');


       }else{
           $.ajax({
               url:"<%=basePath%>Order/GetList.action",
               type:"POST",
               async:false,
               datatype:JSON,
               data:{tableId:tableid},
               success:function (data) {
                   $('table').css("display",'block');
                   $('#defaultmessage').css("display","none");
                   $('tbody').empty();
                 var jsonobj=JSON.parse(data);

                 console.log("windowid="+tableid);
                 var tid=tableid;
                 console.log("tid+"+tid);

                   $('#topay').attr("onclick","topay('"+tid+"')");
                   jsonobj.forEach(showitem);

               }
               
           })
       }

   }
   function showitem(item) {

        console.log(item.goodName);
       var tr=document.createElement("TR"); //创建TR节点
       var name=document.createElement("TD");//创建TD节点
       var sum=document.createElement("TD");
       var num=document.createElement("TD");
       var namep=document.createTextNode(item.goodName); //创建各自的文字节点
       var sump=document.createTextNode(item.sum);
       var nump=document.createTextNode(item.goodNum);
       name.appendChild(namep); //将元素节点连接文字节点
       sum.appendChild(sump);
       num.appendChild(nump);

       $("tbody").append(tr) //将TR连接至tbody，在table中显示出来
       tr.setAttribute("onclick"," clicktr(this)")
       tr.setAttribute("id",item.goodId);
       tr.appendChild(name);
       tr.appendChild(num);
       tr.appendChild(sum);
   }
   function canceltable(){
       $('#bg').css("display", "none");
       $('#toOrder').css("display", "none");
       $('#selected').css("border",'none');
   }
   function boxborder(table) {
       var selected =document.getElementById("selected"); //寻找是否有已经被选择的table
//       $('#selected').css('border','5px solid red');

       if(selected==null){
           table.setAttribute('id','selected');
           table.style.border='5px solid red';
       }else {
           console.log(selected)
           selected.style.border='none';
           $('table').css("display",'none');
           $('#defaultmessage').css("display","block");
           selected.removeAttribute('id');
           table.setAttribute('id','selected');
           table.style.border='5px solid red';
       }


   }

   function topay(id){
       console.log("topayid"+id);
       window.location.href="<%=basePath%>Order/ToPay/"+id+".action";

   }
   function updateorder() {
       var selected=document.getElementById("selected");
       if (selected!=null){
          var ordertable=selected.childNodes[3].firstChild.nodeValue
           window.location.href='<%=basePath%>Order/ToMenu/'+ordertable

       }

   }




</script>