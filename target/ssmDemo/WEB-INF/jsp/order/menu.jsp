<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 2017/11/21
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    String imgPath = basePath + "fileUpload/temp/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style>
        *{
            margin: 0;
            padding: 0;
            list-style: none;
            text-decoration: none;
        }
        a{
            color: black;
        }
        html,body{
            height: 100%;
            width: 100%;
        }
        #show{
            height: 100%;
            width: 20%;

            float: left;
            position: relative;
        }
        #menu{
            height: 100%;
            width: 70%;
            position: absolute;
           right:0;
            top:0;
            background-color: rgba(123,1,123,0.7);
        }
        .good{
            height: 100px;
            width: 100px;
            margin: 10px;
            float: left;
            border: 1px;
            background-color: white;
            text-align: center;
            line-height: 50px;

        }

        #tool{
            width: 10%;
            height:100%;
            float: left;
            background-color: rgba(23,223,123,0.7);
        }
        #toolbox{
            margin-top: 150px;
        }
        .toolitem{
            width: 80%;
            height: 50px;
            margin: 10px auto;
            text-align: center;
            line-height: 50px;
            border:1px solid black;
            font-size: 20px;
        }
        .selected {


            background-color: rgba(251, 99, 54, 0.7);


        }
        table{border-collapse: collapse;
               }
        table tr{border-bottom:solid 1px black;
        overflow: hidden}
        td{
            width: 3%;
            height:40px;
        }

        #sum{
            width: 130px;
            height: 50px;

            position: absolute;
            right: 0;
            bottom:0;
            text-align: center;
            line-height: 50px;
            font-size: 15px;
        }
    </style>


</head>
<body>
<div>
    <a href="">1234</a>
</div>
<div id="show">

    <table>
        <thead>
        <tr>

            <td>菜名</td>
            <td>数量</td>
            <td>价钱</td>
        </tr>
        </thead>
       <tbody>
<c:forEach items="${orderlist}" var="o">
    <tr onclick="clicktr(this)" id=${o.goodId} >
    <td>${o.goodName}</td>
    <td>${o.goodNum}</td>
    <td>${o.goodPrice}</td>
    </tr>
</c:forEach>
       </tbody>
    </table>
        <div id="sum">总计：0</div>



</div>
<div id="tool">
    <a href="javascript:void(0)" onclick="deductnum(this)"><div id="deduct" class="toolitem">-</div></a>
    <div id="shownum" class="toolitem">0</div>
    <a href="javascript:void(0)" onclick="addnum(this)"> <div id="add" class="toolitem">+</div></a>
    <div id="toolbox">

        <a href="<%=basePath%>Order/Top.action"><div class="toolitem">返回</div></a>

        <a href="javascript:void(0)" onclick="order('${tableid}')"><div id="order" class="toolitem">下单</div></a>

    </div>
</div>
<div id="menu">
   <c:forEach  items="${goodList}" var="g">
        <a href="javascript:void (0)" onclick="clickgood(${g.id})">
        <div class="good">
               ${g.name}
                   <br>
                ${g.price}
            </div>
        </a>
    </c:forEach>

</div>

</body>
</html>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
    console.log($('div')[0].childNodes[2])
    var gl= new Array();
    var gl2=new Array();
    var num=0;//计算点的菜的种类的数目
    var sum=0;
    $(function() {
        var i = 0;
        var length = $('tbody').children().length;
        for (i; i < length; i++) {
            var goodid =$('tbody')[0].children[i].getAttribute('id');
      console.log();
            window.gl.push(goodid);

        $.ajax({
        url: '<%=basePath%>Order/AddMenu.action',
        async: false,
        type: 'POST',
        data: {
        id:goodid
        },success:function (data) {
        var json = eval("("+data+")");//将json类型字符串转换为json对象
        window.gl2.push(json);
        var num= parseInt($('tbody')[0].childNodes[1].childNodes[3].firstChild.nodeValue);
        var nsum=parseInt(json.goodPrice)*num;
       window.gl2[i].goodNum =num.toString();
        window.gl2[i].sum=nsum.toString();
        countsum();

        }

        })}

//        console.log($('tbody')[0].childNodes[1].childNodes[3].firstChild.nodeValue)




    })

function clickgood(goodid) {
    var exist=window.gl.indexOf(goodid.toString()); //检测此good是否已经在table里面
    console.log(goodid);
    if (exist>-1) {     //已经在table里面，修改数量与价钱
        console.log(exist);
        var num= parseInt(window.gl2[exist].goodNum)+1;
        var nsum=parseInt(window.gl2[exist].goodPrice)*num;
        window.gl2[exist].goodNum=num.toString();
        window.gl2[exist].sum=nsum.toString();
        $('tbody').empty();
        var num=document.getElementById("shownum");
        num.firstChild.nodeValue=0; //修改shownum里面的数量
        window.gl2.forEach(showitem);
        countsum();//计算总金额

    }else{

        window.gl.push(goodid.toString());
        $.ajax({
            url: '<%=basePath%>Order/AddMenu.action',
            async: true,
            type: 'POST',
            data: {
                id:goodid
            },success:function (data) {
                var json = eval("("+data+")");//将json类型字符串转换为json对象
                console.log(json.goodName);
                window.gl2.push(json);
              $('tbody').empty();
                var num=document.getElementById("shownum");
                num.firstChild.nodeValue=0; //将shownum的数字变回0
                window.gl2.forEach(showitem); //循环输出gl2里面的内容，填充表格
                countsum();

            }


        })
    }
}
    function showitem(item) {


        var tr=document.createElement("TR"); //创建TR节点
        var name=document.createElement("TD");//创建TD节点
        var sum=document.createElement("TD");
        var num=document.createElement("TD");
        var namep=document.createTextNode(item.goodName); //创建各自的文字节点
        var sump=document.createTextNode(item.sum);
        var nump=document.createTextNode(item.goodNum);
        name.appendChild(namep); //将元素节点连接文字节点
        sum.appendChild(sump);
        num.appendChild(nump);

        $("tbody").append(tr) //将TR连接至tbody，在table中显示出来
        tr.setAttribute("onclick","clicktr(this)")
        tr.setAttribute("id",item.goodId);
        tr.appendChild(name);
        tr.appendChild(num);
        tr.appendChild(sum);
    }

   function clicktr(message) {
    var selected =document.getElementsByClassName("selected")[0]; //寻找是否有已经被选择的tr
    if (selected!=null){ //已经有被选择的tr

        selected.removeAttribute("class"); //去除selected class，取消选中变色。
        message.setAttribute("class","selected");//给新选择的tr赋予class SELECTED ，选中变色
    }else {
        message.setAttribute("class","selected");
    }
    var num=document.getElementById("shownum");
       num.firstChild.nodeValue=message.children[1].firstChild.nodeValue; //使shownum显示选中项的数目

       countsum();

   }
   function  deductnum(message) { //修改选中项的数量 （减少）


       if ($('.selected')[0].hasAttribute("class")) { //已经有被选择的tr

           var id = $('.selected').attr("id");//获取被选项的ID
           var index = window.gl.indexOf(id);//获得被选项在数组里的位置，以修改数目
//           console.log(window.gl[0]+'in gl');
//           console.log(window.gl2[0].goodId+'in gl2');
//           if (window.gl[0]===id)
//               console.log(index);
           var num = parseInt(window.gl2[index].goodNum) - 1;//点一下，数目减一
           if (num > 0) {//数目减一后如果还有
               var nsum = parseInt(window.gl2[index].goodPrice) * num;
               window.gl2[index].goodNum = num.toString();
               window.gl2[index].sum = nsum.toString();
               $('#shownum')[0].firstChild.nodeValue = num;//将修改后的数目\价格显示
               $('.selected')[0].children[1].firstChild.nodeValue = num;
               $('.selected')[0].children[2].firstChild.nodeValue = nsum;
               window.sum=0;
               window.gl2.forEach(countsum);
               $('#sum')[0].firstChild.nodeValue="总计："+window.sum;
           } else {//数目减一后已经被减完
               $(".selected").remove();
               window.gl.splice(index, 1);
               window.gl2.splice(index, 1);
               $('#shownum')[0].firstChild.nodeValue = 0;
               countsum();
           }
//           console.log("zuizhong"+window.gl2);

       }
   }
       function  addnum(message) { //修改选中项的数量 （减少）



           if ( $('.selected')[0].hasAttribute("class")) { //已经有被选择的tr

               var id= $('.selected').attr("id");//获取被选项的ID
               var index= window.gl.indexOf(id);//获得被选项在数组里的位置，以修改数目
//           console.log(window.gl[0]+'in gl');
//           console.log(window.gl2[0].goodId+'in gl2');
//           if (window.gl[0]===id)
//               console.log(index);
               var num= parseInt(window.gl2[index].goodNum)+1;//点一下，数目加一
                   var nsum=parseInt(window.gl2[index].goodPrice)*num;
                   window.gl2[index].goodNum=num.toString();
                   window.gl2[index].sum=nsum.toString();
                   $('#shownum')[0].firstChild.nodeValue=num;//将修改后的数目\价格显示
                   $('.selected')[0].children[1].firstChild.nodeValue=num;
                   $('.selected')[0].children[2].firstChild.nodeValue=nsum;
                    countsum();
               }
//           console.log("zuizhong"+window.gl2);
    }

    function order(tableid) {
      $.ajax({
          url: '<%=basePath%>Order/ToDb.action',
          async:false,
          type:"POST",
          dataTyoe:"json",
//          data:  {},
          data:{"sum":window.sum,"tableid":tableid,"json":JSON.stringify(window.gl2),"num":window.num},
          success:function (data) {

              window.location.href='<%=basePath%>Order/Top.action';
          }

        })

    }
    function countsum() {
        window.sum=0;
        window.num=0;
        window.gl2.forEach(foreachsum);//计算总金额
        $('#sum')[0].firstChild.nodeValue="总计："+window.sum;

    }
    function foreachsum(item) {

    var cost=parseInt(item.sum);
    window.sum= window.sum+cost;
    window.num=window.num+1;
    console.log( window.num+'windownum');


    }

</script>

